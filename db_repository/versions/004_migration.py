from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
documents = Table('documents', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('document_name', String(length=64)),
    Column('document_url', String(length=200)),
    Column('document_info', String(length=256)),
    Column('uploader', Integer),
    Column('category', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['documents'].columns['document_info'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['documents'].columns['document_info'].drop()
