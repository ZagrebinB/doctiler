import sqlite3

from flask import Flask, request, redirect, url_for
from flask.ext.sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

from app import models, views
