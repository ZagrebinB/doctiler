import os

import requests
from flask import Flask, g, redirect, url_for, session, abort, request, flash, render_template, send_from_directory
from flask_oauth import OAuth
from urllib2 import Request, urlopen, URLError
from requests_oauthlib import OAuth2Session
from flask.ext.login import LoginManager, login_required, login_user, logout_user
from app import app, models, db
import json
from sqlalchemy.orm import sessionmaker
from werkzeug.utils import secure_filename
from .models import User, Documents, Cafedras
import config


js = json
GOOGLE_CLIENT_ID = '722425777287-nn92ekej14ka530dr7t118ervuhh1ps2.apps.googleusercontent.com'
GOOGLE_CLIENT_SECRET = 'LkE8Gj9pYkjx2u8kBh8EfUf9'
REDIRECT_URI = "/reddit_callback"  # one of the Redirect URIs from Google APIs console


class Auth:
    CLIENT_ID = ('722425777287-nn92ekej14ka530dr7t118ervuhh1ps2'
                 '.apps.googleusercontent.com')
    CLIENT_SECRET = 'LkE8Gj9pYkjx2u8kBh8EfUf9'
    REDIRECT_URI = '/reddit_callback'
    AUTH_URI = 'https://accounts.google.com/o/oauth2/auth'
    TOKEN_URI = 'https://accounts.google.com/o/oauth2/token'
    USER_INFO = 'https://www.googleapis.com/userinfo/v2/me'


GOOGLE_OAUTH2_USERINFO_URL = 'https://www.googleapis.com/oauth2/v2/userinfo'
SECRET_KEY = 'development key'
DEBUG = True

app.secret_key = SECRET_KEY
oauth = OAuth()

google = oauth.remote_app('google',
                          base_url='https://www.google.com/accounts/',
                          authorize_url='https://accounts.google.com/o/oauth2/auth',
                          request_token_url=None,
                          request_token_params={'scope': 'https://www.googleapis.com/auth/userinfo.email',
                                                'response_type': 'code'},
                          access_token_url='https://accounts.google.com/o/oauth2/token',
                          access_token_method='POST',
                          access_token_params={'grant_type': 'authorization_code'},
                          consumer_key=GOOGLE_CLIENT_ID,
                          consumer_secret=GOOGLE_CLIENT_SECRET)

login_manager = LoginManager(app)
login_manager.login_view = "login"
login_manager.session_protection = "strong"
Session = sessionmaker()


class CurrentUser:
    def __init__(self, google_id, email, name, picture):
        self.google_id = google_id
        self.email = email
        self.name = name
        self.picture = picture

    def __repr__(self):
        return self.picture


cur_user = CurrentUser('', '', '', '')
cuser = None

def get_google_auth(state=None, token=None):
    if token:
        return OAuth2Session(Auth.CLIENT_ID, token=token)
    if state:
        return OAuth2Session(
            Auth.CLIENT_ID,
            state=state,
            redirect_uri=Auth.REDIRECT_URI)
    oauth = OAuth2Session(
        Auth.CLIENT_ID,
        redirect_uri=Auth.REDIRECT_URI,
        scope=Auth.SCOPE)
    return oauth


@app.route('/')
def index():
    if authorized:
        return render_template('index.html', user=cur_user, title='Home', cuser=User)
    access_token = session.get('access_token')
    if access_token is None:
        return redirect(url_for('login'))
    access_token = access_token[0]
    headers = {'Authorization': 'OAuth ' + access_token}
    req = Request('https://www.googleapis.com/oauth2/v1/userinfo',
                  None, headers)
    try:
        res = urlopen(req)
        global js
        js = res.read()
    except URLError, e:
        if e.code == 401:
            # Unauthorized - bad token
            session.pop('access_token', None)
            return redirect(url_for('login'))
        return res.read()
    return js


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/login')
def login():
    callback = url_for('authorized', _external=True)
    return google.authorize(callback=callback)


@app.route(REDIRECT_URI)
@google.authorized_handler
def authorized(resp):
    access_token = resp['access_token']
    session['access_token'] = access_token, ''
    userinfo = requests.get(GOOGLE_OAUTH2_USERINFO_URL, params=dict(
        access_token=access_token,
    )).json()
    global cur_user
    cur_user = CurrentUser(str (userinfo['id']), userinfo['email'], userinfo['name'], userinfo['picture'])
    user = User.query.filter_by(google_id=cur_user.google_id).first()
    g.user = user
    if user is None:
        user = models.User(str (userinfo['id']), userinfo['email'], userinfo['name'], userinfo['picture'])
        db.session.add(user)
        db.session.commit()
    login_user(user, remember=True)
    if user.cafedra_id is None or user.faculty_id is None or user.course is None:
        return render_template('registration.html', user=cur_user, title='Registration')
    return redirect(url_for('index'))


@app.route('/logout')
@login_required
def logout():
    global cur_user
    cur_user = CurrentUser('', '', '', '')
    logout_user()
    return redirect(url_for('index', user=cur_user))


@google.tokengetter
def get_access_token():
    return session.get('access_token')


@app.route('/registration', methods=['GET', 'POST'])
@login_required
def registration():
    g.user = User.query.filter_by(google_id=cur_user.google_id).first()
    faculty = request.form.get('Faculty')
    cafedra = request.form.get('Cafedra')
    course = request.form.get('course')
    g.user.faculty_id = faculty
    g.user.cafedra_id = cafedra
    g.user.course = course
    db.session.add(g.user)
    db.session.commit()
    return render_template('index.html', user=cur_user, title='Home')


@app.route('/documents')
@login_required
def documents():
    docs = Documents.query.all()
    cafedra_id = User.query.filter_by(google_id=cur_user.google_id).first().cafedra_id
    return render_template('documents.html', docs = docs, user=cur_user, cafedra = cafedra_id, title='Documents', cuser=User)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in config.ALLOWED_EXTENSIONS

@app.route('/upload', methods=['GET', 'POST'])
@login_required
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            doc_name = request.form.get('doc_name')
            doc_category = request.form.get('category')
            doc_description = request.form.get('description')
            doc_uploader = User.query.filter_by(google_id=cur_user.google_id).first().id
            doc_url = filename
            docs = Documents(doc_name, doc_url, doc_description, doc_uploader, doc_category)
            db.session.add(docs)
            db.session.commit()
            return redirect(url_for('index'))

    return render_template('upload.html', user = cur_user, title='Upload')

@app.route('/download/<filename>')
@login_required
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)