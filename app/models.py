from app import db


class Faculties(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    faculty_name = db.Column(db.String(32))
    user_faculty = db.relationship('User', backref = 'faculty', lazy = 'dynamic')


class Cafedras(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cafedra_name = db.Column(db.String(32))
    user_cafedra = db.relationship('User', backref = 'cafedra', lazy = 'dynamic')
    document_category = db.relationship('Documents', backref = 'doc_category', lazy = 'dynamic')


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    google_id = db.Column(db.String(30))
    email = db.Column(db.String(100))
    name = db.Column(db.String(100))
    avatar = db.Column(db.String(200))
    faculty_id = db.Column(db.Integer, db.ForeignKey('faculties.id'))
    cafedra_id = db.Column(db.Integer, db.ForeignKey('cafedras.id'))
    course = db.Column(db.Integer)
    document_uploader = db.relationship('Documents', backref = 'doc_uploader', lazy = 'dynamic')

    def __init__(self, google_id, email, name, avatar):
        self.google_id = google_id
        self.email = email
        self.name = name
        self.avatar = avatar

    def __repr__(self):
        return '<User %r, email %r>' % (self.name, self.email)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3




class Documents(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    document_name = db.Column(db.String(64), index=True, unique=False)
    document_url = db.Column(db.String(200), index=True, unique=False)
    document_info = db.Column(db.String(256))
    uploader = db.Column(db.Integer, db.ForeignKey('user.id'))
    category = db.Column(db.Integer, db.ForeignKey('cafedras.id'))

    def __init__(self, document_name, document_url, document_info, uploader, category):
        self.document_name = document_name
        self.document_url = document_url
        self.document_info = document_info
        self.uploader = uploader
        self.category = category

